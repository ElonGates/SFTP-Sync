use crypto::digest::Digest;
use crypto::sha2::Sha256;

pub fn folder(buffers: Vec<Vec<u8>>) -> Vec<String> {
    let mut hashes: Vec<String> = Vec::new();

    let mut hasher = Sha256::new();

    for buffer in buffers {
        hasher.input(&buffer);

        hashes.push(hasher.result_str());
        hasher.reset();
    }

    hashes
}

pub fn file(buffer: &Vec<u8>) -> String {
    let mut hasher = Sha256::new();

    //io::copy(&mut file, &mut hasher).expect("Failed to IO copy (no permission?)");

    hasher.input(buffer);

    hasher.result_str()
}

pub fn string(input: &String) -> String {
    let mut hasher = Sha256::new();

    hasher.input_str(input);

    hasher.result_str()
}
