#![allow(non_snake_case)]
#[macro_use]
extern crate dotenv_codegen;

mod shahash;

use dotenv;
use std::fs::{File, OpenOptions};
use std::io::{Read, Write};
use std::env::{self, args};
use std::path::Path;
use ssh2::Session;
use std::net::TcpStream;
use filetime::FileTime;

fn main() {
    dotenv::dotenv().expect(".env file not found");
    dotenv!("SSH_SERVER", "SSH_SERVER was not set in .env");

    let useAgentE = dotenv::var("SSH_AGENT").expect("SSH_AGENT was not set in .env");
    let useAgent = matches!(useAgentE.as_str(), "true" | "t" | "1" | "T" | "True"); // dotenv has no support for true and false :cri

    if !useAgent {
        dotenv!("SSH_PASS", "SSH_PASS was not set in .env");
    }

    let args: Vec<String> = env::args().collect();

    //env::args().nth(2).expect("You need to pass two argument(s)");

    let target = Path::new(&args[1]);
    let serverTarget = Path::new(&args[2]);

    if !target.exists() {
        panic!("Invalid path");
    }

    let server = dotenv::var("SSH_SERVER").unwrap(); // borrowing in rust 
    let serverSplit: Vec<&str> = server.split("@").collect();

    let tcp = TcpStream::connect(serverSplit[1]).unwrap();
    let mut sess = Session::new().unwrap();

    sess.set_tcp_stream(tcp);
    sess.handshake().unwrap();

    if useAgent {
        sess.userauth_agent(serverSplit[0]).unwrap();
    } else {
        sess.userauth_password(serverSplit[0], dotenv::var("SSH_PASS").unwrap().as_str()).unwrap();
    }

    assert!(sess.authenticated());

    let sftp = sess.sftp().unwrap();

    if target.is_file() {
        //let mut file = File::open(target).unwrap();
        let mut file = OpenOptions::new().read(true).write(true).open(target).unwrap();

        let mut buffer: Vec<u8> = Vec::new();
        file.read_to_end(&mut buffer).unwrap();

        let localHash = shahash::file(&buffer);

        if sftp.opendir(&serverTarget).is_err() {
            println!("Could not open {:?}, it will be created", serverTarget.as_os_str());

            sftp.mkdir(&serverTarget, 0o775).unwrap(); // Put the file onto the server since the folder doesnt exist anyway

            //let serverRoot = sftp.opendir(&serverTarget).unwrap();

            sftp.create(&serverTarget.join(target.file_name().unwrap())).unwrap().write_all(&buffer).unwrap();
        } else if sftp.open(&serverTarget.join(target.file_name().unwrap())).is_err() {
            println!("Cannot open file, it will be created");
            sftp.create(&serverTarget.join(target.file_name().unwrap())).unwrap().write_all(&buffer).unwrap();
        } else { // Since the folder does exist along with the file, we need to check the hash
            println!("Checking file hash");
            let mut serverFile = sftp.open(&serverTarget.join(target.file_name().unwrap())).unwrap();

            let mut buffer: Vec<u8> = Vec::new();
            serverFile.read_to_end(&mut buffer).unwrap();

            let serverHash = shahash::file(&buffer);

            if serverHash != localHash {
                let mtimeServer = sftp.stat(&serverTarget.join(target.file_name().unwrap())).unwrap().mtime.unwrap();
                let mtimeLocal = FileTime::from_last_modification_time(&file.metadata().unwrap()).unix_seconds() as u64;

                if mtimeLocal > mtimeServer {
                    sftp.create(&serverTarget.join(target.file_name().unwrap())).unwrap().write_all(&buffer).unwrap();
                } else {
                    let mut buffer: Vec<u8> = Vec::new();
                    serverFile.read_to_end(&mut buffer).unwrap();

                    file.write(&buffer).unwrap();
                }
            }
        }

    } else { // is folder
        let mut buffers: Vec<Vec<u8>> =  Vec::new();

        let path = Path::join(&target, "*");
        let pat = format!("{}", path.display());

        for f in glob::glob(&pat).unwrap() { // Glob uses files inside a folder in alphabetical order 
            if f.as_ref().unwrap().is_file() {
                let mut file = File::open(f.unwrap()).unwrap();

                let mut buffer: Vec<u8> = Vec::new();
                file.read_to_end(&mut buffer).unwrap();

                buffers.push(buffer);
            }
        }

        let hashes = shahash::folder(buffers);

        let localHash = shahash::string(&hashes.join(""));

        if sftp.opendir(&serverTarget.join(target.file_name().unwrap())).is_err() {
            println!("Could not open {:?}, it will be created", serverTarget.join(target.file_name().unwrap()).as_os_str());

            sftp.mkdir(&serverTarget.join(target.file_name().unwrap()), 0o775).unwrap();
        }

        let serverRoot = sftp.opendir(&serverTarget.join(target.file_name().unwrap())).unwrap();

    }
}
